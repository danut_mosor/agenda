﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicatie1
{
    public partial class AgendaForm : Form
    {
        PersonController controller = new PersonController();
        public AgendaForm()
        {
            InitializeComponent();

            controller.People = controller.Dao.getAllPeople();
            showTable();
        }

        private void showTable()
        {
            Table.Rows.Clear();
            Table.Refresh();
        
            if (controller.People.Count != null)
            {
                for (int i = 0; i < controller.People.Count; i++)
                {
                    Table.Rows.Add(controller.People[i].Name, controller.People[i].Phone, controller.People[i].Email);
                }
            }
        }

        private void filter()
        {
            Table.Rows.Clear();
            Table.Refresh();
            int ok = 0;

            for (int i = 0; i < controller.People.Count; i++)
            {
                if (controller.People[i].Name.Contains(filterTextBox.Text) ||
                   controller.People[i].Phone.Contains(filterTextBox.Text))
                {
                    Table.Rows.Add(controller.People[i].Name, controller.People[i].Phone, controller.People[i].Email);
                    ok = 1;
                }
            }
            if (ok == 0)
            {
                showTable();
            }
        }

        private void insertButtton_Click(object sender, EventArgs e)
        {
            controller.People = controller.insertPerson(controller.People, nameTextBox.Text, phoneTB.Text, emailTextBox.Text);
            showTable();
        }

        private void Table_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = Table.CurrentCell.RowIndex;
            nameTextBox.Text = Convert.ToString(Table.Rows[i].Cells[0].Value);
            phoneTB.Text = Convert.ToString(Table.Rows[i].Cells[1].Value);
            emailTextBox.Text = Convert.ToString(Table.Rows[i].Cells[2].Value);
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            controller.People = controller.deletePerson(controller.People, nameTextBox.Text, phoneTB.Text, emailTextBox.Text);
            showTable();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            int index;
            index = Table.CurrentCell.RowIndex;
            controller.People = controller.updatePerson(controller.People, index , nameTextBox.Text, phoneTB.Text, emailTextBox.Text);
            showTable();
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            filter();
        }
    }
}
