﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicatie1
{
    class PersonDAOImpl : PersonDAO
    {

        List<Person> people = new List<Person>();

        public PersonDAOImpl()
        { }

        public void addPerson(Person person)
        {
            using (StreamWriter w = File.AppendText(@"C:\Users\Dan\Desktop\csharp\Aplicatie1\Aplicatie1\people.txt"))
            {   
                w.WriteLine("{0} {1} {2}",person.Name, person.Phone, person.Email);
                w.Flush();
            }
        }

        public List<Person> getAllPeople()
        {
            string line;
            Person person;
            using (StreamReader file = new StreamReader(@"C:\Users\Dan\Desktop\csharp\Agenda\Agenda\people.txt"))
            {
                while ((line = file.ReadLine()) != null)
                {

                    char[] delimiters = new char[] { ' ' };
                    string[] parts = line.Split(delimiters);

                    person = new Person(parts[0], parts[1], parts[2]);               
                    people.Add(person);
                }
                file.Close();
            }
            return people;
        }

        public void updatePerson(List<Person> people)
        {
            using (StreamWriter newTask = new StreamWriter(@"C:\Users\Dan\Desktop\csharp\Agenda\Agenda\people.txt", false))
            {
                for (int i = 0; i < people.Count; i++)
                {
                    newTask.WriteLine("{0} {1} {2}", people[i].Name, people[i].Phone, people[i].Email);
                }     
                newTask.Flush();
            }
        }
    }
}
