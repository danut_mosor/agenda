﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicatie1
{
    interface PersonDAO
    {
        List<Person> getAllPeople();
        void addPerson(Person person);
        void updatePerson(List<Person> people);
        
    }

}

