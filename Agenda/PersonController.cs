﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicatie1
{
    class PersonController
    {
        List<Person> people = new List<Person>();
        PersonDAO dao = new PersonDAOImpl();

        internal List<Person> People
        {
            get
            {
                return people;
            }

            set
            {
                people = value;
            }
        }
        internal PersonDAO Dao
        {
            get
            {
                return dao;
            }

            set
            {
                dao = value;
            }
        }
        public PersonController()
        {
            
        }
        public List<Person> insertPerson(List<Person> people, string name, string phone, String email)
        {
            Person person = new Person(name,phone,email);
            people.Add(person);
            Dao.addPerson(person);
            return people;
        }
        public List<Person> deletePerson(List<Person> people,string name, string phone, string email)
        {
            Person person = new Person(name, phone, email);
            for (int i=0;i<people.Count;i++)
            {
                if ((people[i].Name == person.Name) && (people[i].Phone == person.Phone) && (people[i].Email== person.Email))
                {
                    people.RemoveAt(i);
                    break;
                }
            }
            //people.Remove(person);
            Dao.updatePerson(people);
            return people;
        }
        public List<Person> updatePerson(List<Person> people ,int index , string name, string phone, string email)
        {
            people[index].Name = name;
            people[index].Phone = phone;
            people[index].Email = email;
            Dao.updatePerson(people);
            return people;
        }
    }
}
